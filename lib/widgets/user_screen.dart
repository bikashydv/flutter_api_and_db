import 'package:api/model/db_model.dart';
import 'package:api/model/todo_model.dart';
import 'package:flutter/material.dart';

class UserInput extends StatelessWidget {
  final Function insertFunction;
  final DatabaseConnect db;
  UserInput({required this.insertFunction, key, required this.db})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 8),
      color: const Color(0xFFDAB5FF),
      child: InkWell(
        child: Container(
          color: Colors.red,
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: const Text(
            'Add',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        onTap: () {
          //dialog open
          //insert todos
          //
          // addUpdateDialog(
          //   context,
          //   null,
          //   insertFunction,
          // );
          showDialog(
              context: context,
              builder: (BuildContext context) {
                final TextEditingController textEditingcontroller =
                    TextEditingController();
                return AlertDialog(
                    title: Text("Add new "),
                    content: Column(
                      children: [
                        TextField(
                          controller: textEditingcontroller,
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      ElevatedButton(
                        child: Text("Add new "),
                        onPressed: () async {
                          //insert
                          // insertFunction.call(Todoo(
                          //   title: textEditingcontroller.text,
                          //   creationDate: DateTime.now(),
                          //   isChecked: false,
                          // ));
                          await db.insertTodo(Todoo(
                            title: textEditingcontroller.text,
                            creationDate: DateTime.now(),
                            isChecked: false,
                          ));

                          Navigator.of(context).pop();
                        },
                      )
                    ]);
              });
        },
      ),
    );
  }
}
