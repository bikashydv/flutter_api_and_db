import 'package:api/widgets/todo_list.dart';
import 'package:api/widgets/user_screen.dart';
import 'package:flutter/material.dart';
import 'package:api/model/db_model.dart';
import 'package:api/model/todo_model.dart';

class DB extends StatefulWidget {
  const DB({Key? key}) : super(key: key);

  @override
  State<DB> createState() => _DBState();
}

class _DBState extends State<DB> {
  DatabaseConnect db = DatabaseConnect();

  void addItem(Todoo todoo) async {
    await db.insertTodo(todoo);
    setState(() {});
  }

  void addItem2(Todoo todoo) async {
    setState(() {});
  }
  void updateItem(Todoo todoo) async {
    await db.updatetodo(todoo);
    setState(() {});
  }

  void deleteItem(Todoo todoo) async {
    await db.deleteTodo(todoo);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFF5EBFF),
      appBar: AppBar(
        title: const Text("DB Todo"),
        backgroundColor: Colors.blueGrey,
      ),
      body: Column(children: [
        Todolist(
            insertFunstion: addItem,
            deleteFunction: deleteItem,
            updateFunction: updateItem,db:db),
        UserInput(
          insertFunction: addItem2,
          db:db,
        )
      ]),
    );
  }
}
