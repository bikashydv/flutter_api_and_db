import 'dart:convert';

import 'package:api/model/todo.dart';
import 'package:api/repository/repository.dart';
import 'package:http/http.dart' as http;

class TodoRepository implements Repository {
  String dataURL = 'https://jsonplaceholder.typicode.com';

//delete
  @override
  Future<String> deletedTodo(Todo todo) async {
    var url = Uri.parse('$dataURL/todos/${todo.id}');
    var result = 'false';
    await http.delete(url).then((value) {
      // ignore: avoid_print
      print(value.body);
      //temp
      return result = 'true';
    });
    return result;
  }

//get example

  @override
  Future<List<Todo>> getTodoList() async {
    List<Todo> todoList = [];
    var url = Uri.parse('$dataURL/todos');
    var response = await http.get(url);
    print('status code : ${response.statusCode}');
    var body = json.decode(response.body);
    //parse
    for (var i = 0; i < body.length; i++) {
      todoList.add(Todo.fromJson(body[i]));
    }
    return todoList;
  }

//pacth example

//pathc-> modify passed variable only.

  @override
  Future<String> patchCompleted(Todo todo) async {
    todo.title = DateTime.now().toString(); ////

    var url = Uri.parse('$dataURL/todos/${todo.id}');
    //call back data
    String resData = '';
    //bool? string
    await http.patch(
      url,
      body: {
        'completed': (!todo.completed!).toString(),
      },
      headers: {'Authorization': 'your_token'},
    ).then((response) {
      //homescreen data
      Map<String, dynamic> result = json.decode(response.body);
      // print(result);
      result.forEach((key, value) {
        print('key$key value$value');
      });
      return resData = result['title'];
    });
    return resData;
  }

//put

//Modify passed variable only and treat orther  variable Null or Default
  @override
  Future<String> putCompleted(Todo todo) async {
    var url = Uri.parse('$dataURL/todos/${todo.id}');
    String resData = '';
    await http.put(
      url,
      body: {
        'completed': (!todo.completed!).toString(),
      },
      headers: {'Authorization': 'your_token'},
    ).then((response) {
      Map<String, dynamic> result = json.decode(response.body);
      print(result);
      return resData = result['completed'];
    });
    return resData;
  }

  //post

  @override
  Future<String> postTodo(Todo todo) async {
    print('${todo.toJson()}');
    var url = Uri.parse('$dataURL/todos/');
    var result = '';
    var response = await http.post(url, body: todo.toJson());
    //Fake server => get return type !=post type
    //change tojson method
    //success
    print(response.statusCode);
    print(response.body);
    return 'true';
  }
}
