import 'dart:developer';

import 'package:api/model/todo_model.dart';
import 'package:flutter/material.dart';

class TodocardWidget extends StatefulWidget {
  final void Function(Todoo todoo) insertFunction;
  final void Function(Todoo todoo) deleteFunction;
  final void Function(Todoo todoo) updateFunction;
  final Todoo todoo;
  const TodocardWidget({
    //   required this.id,
    // required this.title,
    // // required this.description,
    // required this.creationDate,
    // required this.isChecked,
    required this.insertFunction,
    required this.deleteFunction,
    required this.updateFunction,
    Key? key,
    required this.todoo,
  }) : super(key: key);

  @override
  State<TodocardWidget> createState() => _TodocardWidgetState();
}

class _TodocardWidgetState extends State<TodocardWidget> {
  late Todoo _todoo;
  @override
  void initState() {
    super.initState();
    _todoo = widget.todoo;
  }

  @override
  Widget build(BuildContext context) {
    print(_todoo.toString());
    return Card(
      child: Row(
        children: [
          Container(
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: IconButton(
                icon: const Icon(Icons.edit),
                onPressed: () {
                  addUpdateDialog(
                    context,
                    _todoo,
                    widget.updateFunction,
                  );
                },
              )),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  _todoo.title,
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 18,
                  ),
                ),
                const SizedBox(height: 5),
                Text(
                  _todoo.creationDate.toString(),
                  style: const TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Color(0XFF8F8F8F),
                  ),
                ),
              ],
            ),
          ),
          IconButton(
            onPressed: () {
              widget.deleteFunction(_todoo);
            },
            icon: const Icon(Icons.close),
          ),
        ],
      ),
    );
  }
}

Future<dynamic> addUpdateDialog(
  BuildContext context,
  Todoo? todoo,
  void Function(Todoo todoo) changeFunction,
) {
  final TextEditingController textEditingcontroller =
      TextEditingController(text: todoo?.title ?? "");
  Todoo? todoos = todoo;
  log(changeFunction.runtimeType.toString());
  return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text(todoo?.title ?? "Add new "),
            content: Column(
              children: [
                TextField(
                  controller: textEditingcontroller,
                ),
              ],
            ),
            actions: <Widget>[
              ElevatedButton(
                child: Text(todoo == null ? "Add new " : "update"),
                onPressed: () {
                  if (todoos == null) {
                    //insert
                    todoos = Todoo(
                      title: textEditingcontroller.text,
                      creationDate: DateTime.now(),
                      isChecked: false,
                    );
                    changeFunction(todoos!);
                  } else {
                    // update
                    todoos!.title = textEditingcontroller.text;
                    changeFunction(todoos!);
                  }
                  Navigator.of(context).pop();
                },
              )
            ]);
      });
}
