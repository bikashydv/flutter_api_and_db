import 'package:api/model/todo.dart';
import 'package:api/repository/repository.dart';

class TodoController {
  final Repository _repository;

  TodoController(this._repository);
  

  //get
  
  Future<List<Todo>> fetchTodoList() async {
    return _repository.getTodoList();
  }

  //patch

  Future<String> updatePatchCompleate(Todo todo) async {
    return _repository.patchCompleted(todo);
  }

  //put
  
  Future<String> updatePutCompleate(Todo todo) async {
    return _repository.putCompleted(todo);
  }

  //del

  Future<String> deletedTodo(Todo todo) async {
    return _repository.putCompleted(todo);
  }

//post

  Future<String> postTodo(Todo todo) async {
    return _repository.postTodo(todo);
  }
}
