class Todoo {
  int? id;
  String title;
  // String description;
  DateTime creationDate;
  bool isChecked;

  Todoo({
    this.id,
    required this.title,
    required this.creationDate,
    required this.isChecked,
    // required this.description,
  });

  Map<String, dynamic> toMap() {
    return {
      'id':id,
      'title': title,
      // 'description': description,
      'creationDate': creationDate.toString(),
      'isChecked': isChecked ? 1 : 0,
    };
  }

  @override
  String toString() {
    return 'Todoo(id :$id, title: $title, ,creationDate : $creationDate, isChecked : $isChecked)';
  }

  //description: $description
}
