import 'package:api/model/db_model.dart';
import 'package:api/model/todo_model.dart';
import 'package:api/widgets/todo_card.dart';
import 'package:flutter/material.dart';

class Todolist extends StatelessWidget {
  final void Function(Todoo todoo) insertFunstion;
  final void Function(Todoo todoo) deleteFunction;
  final void Function(Todoo todoo) updateFunction;

  final DatabaseConnect db ;
  Todolist(
      {required this.insertFunstion,
      required this.deleteFunction,
      required this.db,
      Key? key,
      required this.updateFunction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: FutureBuilder(
      future: db.getTodo(),
      initialData: const [],
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        var data = snapshot.data;
        var datalength = data!.length;
        return datalength == 0
            ? const Center(
                child: Text('no data found'),
              )
            : ListView.builder(
              shrinkWrap: true,
                itemCount: datalength,
                itemBuilder: (context, i) => TodocardWidget(
                    // id: data[i].id,
                    // title: data[i].title,
                    // // description:data[i].description,
                    // creationDate: data[i].creationDate,
                    // isChecked: data[i].isChecked,
                    insertFunction: insertFunstion,
                    deleteFunction: deleteFunction,
                    updateFunction: updateFunction,
                    
                    todoo:data[i],
                    ),
              );
      },
    ));
  }
}
