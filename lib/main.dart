// ignore_for_file: unused_import

import 'package:api/screens/db_screen.dart';
import 'package:flutter/material.dart';
import 'model/db_model.dart';
import 'model/todo_model.dart';
import 'screens/buttons.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const Application());
}

class Application extends StatelessWidget {
  const Application({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // primarySwatch: Colors.blue,
        primaryColor: Colors.orange[80],
        appBarTheme: const AppBarTheme(
          elevation: 0.0,
        ),
      ),
      home: const Buttons(),
    );
  }
}
