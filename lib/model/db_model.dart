import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import './todo_model.dart';

class DatabaseConnect {
  Database? _database;
  Future<Database> get getDB async {
    if(_database==null){
        final dbpath = await getDatabasesPath();
    const dbname = 'todo.db';
    final path = join(dbpath, dbname);
    _database = await openDatabase(path, version: 2, onCreate: _createDB);
    }
  
    return _database!;
  }

  Future<void> _createDB(Database db, int version) async {
    await db.execute('''
   CREATE TABLE Todo(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    title TEXT,
    creationDate TEXT,
    isChecked INTEGER
   )
   ''');
  }

  Future<Todoo> updatetodo(Todoo todoo) async {
    final db = await getDB;
    await db.update(
      'todo',
      where: 'id == ?',
      whereArgs: [todoo.id],
      todoo.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return todoo;
  }

  Future<void> insertTodo(Todoo todoo) async {
     await (await getDB).insert(
      'todo',
      todoo.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteTodo(Todoo todoo) async {
    final db = await getDB;
    await db.delete(
      'todo',
      where: 'id == ?',
      whereArgs: [todoo.id],
    );
  }

//fetch all

  Future<List<Todoo>> getTodo() async {
    final db = await getDB;
    List<Map<String, dynamic>> items = await db.query(
      'todo',
      orderBy: 'id DESC',
    );

    return List.generate(
      items.length,
      (i) => Todoo(
        id: items[i]['id'],
        title: items[i]['title'],
        // description: items[i]['description'],
        creationDate: DateTime.parse(items[i]['creationDate']),
        isChecked: items[i]['ischecked'] == 1 ? true : false,
      ),
    );
  }
}
