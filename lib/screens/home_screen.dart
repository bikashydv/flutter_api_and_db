import 'package:api/controller/todo_controller.dart';
import 'package:api/model/todo.dart';
import 'package:api/repository/todo_repository.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override

  Widget build(BuildContext context) {
    //dependency injection
    var todoController = TodoController(TodoRepository());
    //test
    // todoController.fetchTodoList();

    return Scaffold(
      appBar: AppBar(
        title: Text('REST API'),
      ),
      body: FutureBuilder<List<Todo>>(
        future: todoController.fetchTodoList(),
        builder: (context, snapshot) {
          //
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.hasError) {
            return const Center(
              child: Text('error'),
            );
          }
          return buildBodyContent(snapshot, todoController);
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          //make post call
          //temp data

          Todo todo = Todo(userId: 3, title: 'sample post', completed: false);
          todoController.postTodo(todo);
        },
        child: Icon(Icons.add),
      ),
    );
  }

  SafeArea buildBodyContent(
      AsyncSnapshot<List<Todo>> snapshot, TodoController todoController) {
    return SafeArea(
      child: ListView.separated(
          itemBuilder: (context, index) {
            var todo = snapshot.data?[index];
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 100.0,
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(children: [
                  Expanded(flex: 1, child: Text('${todo?.id}')),
                  Expanded(flex: 2, child: Text('${todo?.title}')),
                  Expanded(
                    flex: 3,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        InkWell(
                            onTap: () {
                              //make snakbar
                              todoController
                                  .updatePatchCompleate(todo!)
                                  .then((value) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    duration: const Duration(microseconds: 100),
                                    content: Text('$value'),
                                  ),
                                );
                              });
                            },
                            child:
                                buildCallContainer('patch', Color(0xFFFFE0B2))),

                        //make put call
                        InkWell(
                            onTap: () {
                              todoController.updatePutCompleate(todo!);
                            },
                            child:
                                buildCallContainer('put', Color(0xFFE1BEE7))),

                        //make a delete
                        InkWell(
                            onTap: () {
                              todoController.deletedTodo(todo!).then((value) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    duration: const Duration(microseconds: 100),
                                    content: Text('$value'),
                                  ),
                                );
                              });
                            },
                            child:
                                buildCallContainer('del', Color(0xFFFFCDD2))),
                      ],
                    ),
                  ),
                ]),
              ),
            );
          },
          separatorBuilder: (context, index) {
            return const Divider(
              thickness: 0.5,
              height: 0.5,
            );
          },
          itemCount: snapshot.data?.length ?? 0),
    );
  }

  Container buildCallContainer(String title, Color color) {
    return Container(
      width: 40.0,
      height: 40.0,
      decoration: BoxDecoration(
          color: color, borderRadius: BorderRadius.circular(10.0)),
      child: Center(child: Text('$title')),
    );
  }
}
